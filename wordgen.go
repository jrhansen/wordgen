package wordgen

import (
	"bufio"
	"io"
	"log"
	"os"
	"strings"
)

type WordGenerator interface {
	Find(letters string, results chan string)
}

type wordGenerator struct {
	words []string
}

func New(filename string) WordGenerator {
	wg := new(wordGenerator)
	if err := wg.loadWords(filename); err != nil {
		log.Print(err)
	}
	return wg
}

func (wg *wordGenerator) loadWords(filename string) (err error) {
	var file *os.File
	if file, err = os.Open(filename); err != nil {
		return err
	}
	defer file.Close()
	reader := bufio.NewReader(file)

	eof := false
	for !eof {
		var line string
		line, err = reader.ReadString('\n')
		if err == io.EOF {
			err = nil
			eof = true
		} else if err != nil {
			return err
		}
		line = strings.TrimSpace(line)
		if line != "" {
			wg.words = append(wg.words, line)
		}
	}

	return err
}

func (wg *wordGenerator) Find(letters string, results chan string) {
	go func() {
		for _, word := range wg.words {
			if canWordBeMadeFromLetters(word, []rune(letters)) {
				results <- word
			}
		}
		close(results)
	}()
}

func canWordBeMadeFromLetters(word string, letters []rune) bool {
	remaining := make([]rune, len(letters), len(letters))
	copy(remaining, letters)
	for _, letter := range word {
		foundI := findLetter(remaining, letter)
		if foundI < 0 {
			return false
		}
		remaining = removeLetter(remaining, foundI)
	}
	return true
}

func findLetter(letters []rune, letter rune) int {
	for i, l := range letters {
		if l == letter {
			return i
		}
	}
	return -1
}

func removeLetter(letters []rune, pos int) []rune {
	return append(letters[:pos], letters[pos+1:]...)
}
