package wordgen

import (
  "testing"
)

func TestRemoveLetter(t *testing.T) {
  letters := []rune{'t','e','s','t'}
  letters = removeLetter(letters, 1)
  if len(letters) != 3 || 
     letters[0] != 't' ||
     letters[1] != 's' ||
     letters[2] != 't'  {
    t.Fail()
  } 
}

